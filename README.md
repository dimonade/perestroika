# Perestroika
## Description
Perestroika is a perhaps naive library that provides primitives to design simulations that are based on genetic algorithms.

It can be used to generate Genomes (i.e., Brains) from Nodes and Connections and simulate propagation from given input to output.
It is also possible to simulate mutation events which are the main engine of evolution.

## Usage
Add this to your `Cargo.toml`:
```toml
[dependencies]
perestroika = "0.1"
```
Or
```bash
cargo add perestroika
```

## Examples

See the crate's [documentation](https://docs.rs/perestroika/latest/perestroika/) for the docs.

Check out the `examples` directory for examples to get going.

## Notes
The current roadmap is to figure an API within the 0.1.* versions and stabilize it.

This means that until 0.2, the API and functionality will probably change, break or differ in one way or another between versions.

## Issues, bugs, suggestions

For issues, bugs, suggestions, comments or anything of the sort feel free to open a new issue on the repository.

[Perestroika's Zulip](https://perestroika.zulipchat.com/) might be a place for more direct communication, although it is pretty empty at the moment.

## Motivation
The motivation behind this project is to make a full scale project in [Rust](https://www.rust-lang.org), 
while integrating many more other fields such as game development in [Bevy](https://bevyengine.org), DevOps (GitLab CI/CD) and, of course, neural networks.
## Uses
Currently being used in an in-development game that I prototype in my spare time.

More projects that employ *perestroika* will be listed below.

## Background
"_Perestroika_" is the Russian word for "_Reconstruction_" and serves as a double pun: 
[Perestroika](https://en.wikipedia.org/wiki/Perestroika) was a late USSR political movement trying to reconstruct the regime;
it is also a very fitting description for [Genetic recombination](https://en.wikipedia.org/wiki/Genetic_recombination) (or genetic reshuffling)
which is the target of this library.

## Bibliography and sources
[Efficient Evolution of Neural Networks through Complexification](http://nn.cs.utexas.edu/downloads/papers/stanley.phd04.pdf) as the paper and background.

[The Bibites](https://the-bibites.fandom.com/wiki/Bibites) provide a good overview of an already implemented game that follows similar principles.

