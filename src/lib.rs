//! Perestroika - A library for simulating evolution.
//!
//! This crate aims to help the users to create "Brains", or in Perestroika's terminology, [`Genome`]s.
//! `Genome`s are essentially self contained decision making machines - given an input, they will
//! propagate it, and return a decision.
//!
//! `Genome`s consist of [`NodeGene`]s split between (or aggregated into) different [`Layer`]s.
//! In order to be able to propagate the inputs the `NodeGenes` must be connected via
//! [`ConnectionGene`]s.
//! As in a real brain, not all `NodeGenes` must be connected to each other or the next layer.
//!
//! `Genomes` also have the ability to mutate.
//! If `NodeGenes` and `ConnectionGenes` are the building blocks of the brain, then mutation is the
//! engine of evolution.
//!
//! Since the crate relies heavily on randomness, in many examples a specific seed is used for the
//! sake of reproducibility, this is an intended use case of the crate as well.
//!
//! # Quick start
//!
//! To generate the smallest [`Genome`] with an input and an output [`Layer`]s without any
//! [`ConnectionGene`]s:
//!
//! ```
//! use rand::SeedableRng;
//! use rand_chacha::ChaCha8Rng;
//! use perestroika::{
//!     errors::PerestroikaError,
//!     genome::Genome,
//!     genome_builder::GenomeBuilder
//! };
//! // This genome uses ChaCha8Rng as the random number generator.
//! // Let's also set a seed for reproducibility:
//! let rng: ChaCha8Rng = ChaCha8Rng::seed_from_u64(42);
//! // It is a very simple genome, consisting of a single input NodeGene and
//! // a single output NodeGene.
//! let mut genome: Genome<ChaCha8Rng> = GenomeBuilder::new()
//!     .with_shape(&[1 ,1])?
//!     .with_random_number_generator(rng)
//!     .build()?;
//!
//! // NodeGenes by default use the Identity activation function and have a bias of 0.
//! // This means, they pass the input onward to the next one, without changing it.
//! let output = genome.propagate(&[0.42])?;
//! // However, the two NodeGenes are not connected, therefore the output is going to be 0:
//! assert_eq!(output, &[0.0]);
//!
//! // Let's connect the two NodeGenes and try to propagate the input again:
//! genome.mutate_connection_add()?;
//! let another_output = genome.propagate(&[0.42])?;
//! // Success!
//! assert_ne!(another_output, &[0.0]);
//!
//! # Ok::<(), PerestroikaError>(())
//! ```

#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![deny(clippy::missing_docs_in_private_items)]
// TODO: Check whether needed or not.
#![allow(clippy::must_use_candidate)]
// TODO: Check whether needed or not.
#![allow(clippy::missing_const_for_fn)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![deny(missing_docs)]

pub mod connection_gene;
pub mod connection_gene_builder;
pub mod errors;
pub mod genome;
pub mod genome_builder;
pub mod layer;
pub mod mutator;
pub mod node_gene;
pub mod node_gene_builder;

pub use crate::connection_gene::ConnectionGene;
pub use crate::connection_gene_builder::ConnectionGeneBuilder;
pub use crate::errors::PerestroikaError;
pub use crate::genome::Genome;
pub use crate::genome_builder::GenomeBuilder;
pub use crate::layer::{Layer, LayerBuilder};
pub use crate::node_gene::NodeGene;
pub use crate::node_gene_builder::NodeGeneBuilder;
