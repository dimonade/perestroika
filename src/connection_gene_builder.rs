//! Connection gene builder functionality.

use rand::{thread_rng, Rng};

use crate::{connection_gene::ConnectionGene, errors::PerestroikaError};

/// See the documentation for [`ConnectionGene`].
#[allow(clippy::missing_docs_in_private_items)]
pub struct ConnectionGeneBuilder {
    source: Option<usize>,
    target: Option<usize>,
    weight: Option<f64>,
    enabled: Option<bool>,
    energy_cost: Option<usize>,
}

impl Default for ConnectionGeneBuilder {
    fn default() -> Self {
        Self {
            source: None,
            target: None,
            weight: Some(thread_rng().gen_range(-1.0..1.0)),
            enabled: Some(true),
            energy_cost: Some(0),
        }
    }
}

impl ConnectionGeneBuilder {
    /// Create a default implementation of a [`ConnectionGene`].
    pub fn new() -> Self {
        Self::default()
    }

    /// Adds a `source` value to the [`ConnectionGene`].
    pub fn with_source(&mut self, source: usize) -> &mut Self {
        self.source = Some(source);
        self
    }

    /// Adds a `target` value to the [`ConnectionGene`].
    pub fn with_target(&mut self, target: usize) -> &mut Self {
        self.target = Some(target);
        self
    }

    /// Adds a `weight` value to the [`ConnectionGene`].
    pub fn with_weight(&mut self, weight: f64) -> &mut Self {
        self.weight = Some(weight);
        self
    }

    /// Adds an `enabled` status to the [`ConnectionGene`].
    pub fn with_enabled_status(&mut self, enabled: bool) -> &mut Self {
        self.enabled = Some(enabled);
        self
    }

    /// Adds an `energy_cost` value to the [`ConnectionGene`].
    pub fn with_energy_cost(&mut self, energy_cost: usize) -> &mut Self {
        self.energy_cost = Some(energy_cost);
        self
    }

    /// Builds the [`ConnectionGene`].
    ///
    /// # Errors
    ///
    /// The method will return an error on a number of scenarios:
    /// 1. There is no `source`.
    /// 2. There is no `target`.
    /// 3. There is no `weight`.
    /// 4. There is no `enabled` status.
    ///
    /// # Examples
    ///
    /// ```
    /// use perestroika::{
    ///     connection_gene::ConnectionGene,
    ///     connection_gene_builder::ConnectionGeneBuilder,
    ///     errors::PerestroikaError,
    /// };
    /// let connection: ConnectionGene = ConnectionGeneBuilder::new()
    ///     .with_source(0)
    ///     .with_target(1)
    ///     .with_weight(0.5)
    ///     .with_enabled_status(true)
    ///     .with_energy_cost(1)
    ///     .build()?;
    ///
    /// # Ok::<(), PerestroikaError>(())
    /// ```
    pub fn build(&self) -> Result<ConnectionGene, PerestroikaError> {
        let Some(source) = self.source else {
            return Err(PerestroikaError::ConnectionGeneMissingSource);
        };
        let Some(target) = self.target else {
            return Err(PerestroikaError::ConnectionGeneMissingTarget);
        };
        let Some(weight) = self.weight else {
            return Err(PerestroikaError::ConnectionGeneMissingWeight);
        };
        let Some(enabled) = self.enabled else {
            return Err(PerestroikaError::ConnectionGeneMissingEnabledStatus);
        };

        Ok(ConnectionGene {
            source,
            target,
            weight,
            enabled,
            energy_cost: self.energy_cost,
        })
    }
}
