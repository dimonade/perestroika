//! Node gene functionality aggregator.

use std::{cell::RefCell, fmt, rc::Weak};

use rand::{distributions::Standard, prelude::Distribution, Rng};

/// The default value of the bias that the [`NodeGene`] initializes with.
pub const NODE_DEFAULT_BIAS: f64 = 0.0;
/// The default value of the mass that the [`NodeGene`] initializes with.
pub const NODE_DEFAULT_MASS: f64 = 0.0;
/// Default limits for the range that the bias may be shifted in due to a mutation.
pub const NODE_DEFAULT_BIAS_SHIFT_LIMITS: f64 = 0.2;

/// The ELU hyperparameter which controls the value to which an ELU saturates for negative net
/// inputs, <https://arxiv.org/pdf/1511.07289.pdf>.
const ELU_ALPHA: f64 = 1.0;
/// A SELU hyperparameter, <https://arxiv.org/pdf/1706.02515.pdf>
const SELU_LAMBDA: f64 = 1.0507;
/// A SELU hyperparameter, <https://arxiv.org/pdf/1706.02515.pdf>.
const SELU_ALPHA: f64 = 1.67326;

/// An enum of predefined activation functions that a [`NodeGene`] can use.
///
/// A list of some of the function used can be found on wikipedia:
/// <https://en.wikipedia.org/wiki/Activation_function#Table_of_activation_functions>.
#[derive(Clone, Debug, Default)]
pub enum ActivationFunction {
    ///
    #[default]
    Identity,
    ///
    BinaryStep,
    ///
    Sigmoid,
    ///
    Tanh,
    ///
    ReLU,
    ///
    GELU,
    ///
    Softplus,
    ///
    ELU,
    ///
    SELU,
    ///
    LeakyReLU,
    ///
    SiLU,
    ///
    Gaussian,
    ///
    Cosine,
}
impl fmt::Display for ActivationFunction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{self:?}")
    }
}

impl Distribution<ActivationFunction> for Standard {
    fn sample<R: Rng + ?Sized>(&self, randy: &mut R) -> ActivationFunction {
        // FIX: Hardcoded size of `ActivationFunction`'s size needs to be avoided.
        match randy.gen_range(0..=12) {
            0 => ActivationFunction::Identity,
            1 => ActivationFunction::BinaryStep,
            2 => ActivationFunction::Sigmoid,
            3 => ActivationFunction::Tanh,
            4 => ActivationFunction::ReLU,
            5 => ActivationFunction::GELU,
            6 => ActivationFunction::Softplus,
            7 => ActivationFunction::ELU,
            8 => ActivationFunction::SELU,
            9 => ActivationFunction::LeakyReLU,
            10 => ActivationFunction::SiLU,
            11 => ActivationFunction::Gaussian,
            _ => ActivationFunction::Cosine,
        }
    }
}

/// Describes the depth that the [Layer](`crate::layer::Layer`) belongs to.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum DepthType {
    /// The input layer.
    Input,
    /// A hidden layer with a depth of N.
    Hidden(usize),
    /// The output layer.
    Output,
}

impl PartialOrd for DepthType {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for DepthType {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match (self, other) {
            (Self::Input, Self::Input) | (Self::Output, Self::Output) => std::cmp::Ordering::Equal,
            (Self::Hidden(left), Self::Hidden(right)) => match left.cmp(right) {
                std::cmp::Ordering::Greater => std::cmp::Ordering::Greater,
                std::cmp::Ordering::Less => std::cmp::Ordering::Less,
                std::cmp::Ordering::Equal => std::cmp::Ordering::Equal,
            },
            (Self::Input, Self::Hidden(_) | Self::Output) | (Self::Hidden(_), Self::Output) => {
                std::cmp::Ordering::Less
            }
            (Self::Hidden(_), Self::Input) | (Self::Output, Self::Input | Self::Hidden(_)) => {
                std::cmp::Ordering::Greater
            }
        }
    }
}

/// A [`NodeGene`] is a sturct contaning the information of a node in a
/// [Genome](crate::genome::Genome).
#[derive(Clone, Debug)]
pub struct NodeGene {
    /// The [`NodeGene`]'s index.
    pub index: usize,
    /// The [`NodeGene`]'s type. Input nodes are marked as "`input_node_gene`", output nodes as
    /// "`output_node_gene`".
    pub depth: DepthType,
    /// The [`NodeGene`]'s energy cost.
    pub energy_cost: Option<usize>,
    /// The [`NodeGene`]'s bias value.
    pub bias: f64,
    /// The [`NodeGene`]'s mass is calculated by adding the bias to the current mass and passing it
    /// through an activation function (for implementation, see [`NodeGene::activate_node`]).
    pub mass: f64,
    /// The [`NodeGene`]'s activation function.
    pub activation_function: ActivationFunction,
    /// The [`NodeGene`]'s [`Vec`] of other [`NodeGene`]'s that it is connected to.
    pub connections_from: Vec<Weak<RefCell<NodeGene>>>,
}

impl fmt::Display for NodeGene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "NodeGene: {{\n\tindex: {:?}, of type {:?}, with mass {}, bias {} and activation function `{}`.\
                \n\tConnected to: {:?}}}",
            self.index,
            self.depth,
            self.mass,
            self.bias,
            self.activation_function,
            self.connections_from
        )
    }
}

impl NodeGene {
    /// Activate the [`NodeGene`].
    ///
    /// Activating is performed by adding a `bias` to the `mass` of the [`NodeGene`] and then
    /// passing it through the `activation_function` of the [`NodeGene`].
    ///
    /// For more information:
    /// <https://en.wikipedia.org/wiki/Activation_function#Table_of_activation_functions>
    ///
    pub fn activate_node(&mut self) {
        self.mass += self.bias;

        match self.activation_function {
            // NOTE: GELU might be too computationally expensive, perhaps approximation? Or remove?
            ActivationFunction::Identity | ActivationFunction::GELU => {}
            ActivationFunction::BinaryStep => {
                if self.mass < 0.0 {
                    self.mass = 0.0;
                } else {
                    self.mass = 1.0;
                }
            }
            ActivationFunction::Sigmoid => {
                // NOTE: Maybe shift it down to be in range [-1.0, 1.0] on the y axis.
                // Now it is [0.0, 1.0].
                self.mass = 1.0 / (1.0 + self.mass.exp());
            }
            ActivationFunction::Tanh => {
                self.mass =
                    (self.mass.exp() - (-self.mass).exp()) / (self.mass.exp() + (-self.mass).exp());
            }
            ActivationFunction::ReLU => {
                if self.mass <= 0.0 {
                    self.mass = 0.0;
                }
            }
            ActivationFunction::Softplus => {
                self.mass = self.mass.exp().ln_1p();
            }
            ActivationFunction::ELU => {
                if self.mass <= 0.0 {
                    self.mass = ELU_ALPHA * self.mass.exp_m1();
                }
            }
            ActivationFunction::SELU => {
                if self.mass < 0.0 {
                    self.mass = SELU_LAMBDA * SELU_ALPHA * self.mass.exp_m1();
                } else {
                    self.mass *= SELU_LAMBDA;
                }
            }
            ActivationFunction::LeakyReLU => {
                if self.mass < 0.0 {
                    self.mass *= 0.01;
                }
            }
            ActivationFunction::SiLU => {
                self.mass = self.mass / (1.0 + (-self.mass).exp());
            }
            ActivationFunction::Gaussian => {
                self.mass = (-self.mass * self.mass).exp();
            }
            ActivationFunction::Cosine => {
                self.mass = self.mass.cos();
            }
        }
        self.mass = self.mass.clamp(-1.0, 1.0);
    }
}
