//! A collection of applicable mutations.

use std::fmt;

use rand::distributions::Standard;
use rand::prelude::Distribution;
use rand::Rng;

/// The [`Mutation`] enum is a collection of available mutation that the
/// [Genome](`crate::genome::Genome`) can perform on itself.
#[derive(Clone, Debug)]
pub enum Mutation {
    /// Adds a new [NodeGene](`crate::node_gene::NodeGene`), if possible.
    NodeAdd,
    /// Removes an existing [NodeGene](`crate::node_gene::NodeGene`), if possible.
    /// Input and output layers' [NodeGene](`crate::node_gene::NodeGene`)s are not affected by this mutation.
    NodeRemove,
    /// Shifts the `bias` value of a [NodeGene](`crate::node_gene::NodeGene`) between the values of
    /// -[limit](`crate::node_gene::NODE_DEFAULT_BIAS_SHIFT_LIMITS`)..[limit](crate::node_gene::NODE_DEFAULT_BIAS_SHIFT_LIMITS).
    NodeShiftBias,
    /// Randomizes the `bias` value of a [NodeGene](`crate::node_gene::NodeGene`).
    NodeRandomizeBias,
    /// Flips the sign of the `bias` value of a [NodeGene](`crate::node_gene::NodeGene`).
    NodeFlipBiasSign,
    /// Changes the [Activation function](`crate::node_gene::ActivationFunction`) of a
    /// [NodeGene](`crate::node_gene::NodeGene`).
    NodeChangeActivationFunction,
    /// Inserts a [NodeGene](crate::node_gene::NodeGene) into a
    /// [ConnectionGene](`crate::connection_gene::ConnectionGene`), if possible.
    ///
    /// This effectively breaks a [ConnectionGene](`crate::connection_gene::ConnectionGene`)
    /// into two, and inserts a [NodeGene](crate::node_gene::NodeGene) into it, while preserving
    /// the route of the previous `source` to the `target` through the newly created
    /// [NodeGene](`crate::node_gene::NodeGene`).
    NodeInsertIntoConnection,
    /// Shifts the `bias` value of all [NodeGenes](`crate::node_gene::NodeGene`).
    NodesShiftAllBiases,
    /// Randomizes the `bias` value of all [NodeGenes](`crate::node_gene::NodeGene`).
    NodesRandomizeAllBiases,
    /// Flips the `bias` value of all [NodeGenes](`crate::node_gene::NodeGene`).
    NodesFlipAllBiasSigns,
    /// Changes the [Activation function](`crate::node_gene::ActivationFunction`) of all
    /// [NodeGenes](`crate::node_gene::NodeGene`).
    NodesChangeAllActivationFunctions,
    /// Adds a new [ConnectionGene](`crate::connection_gene::ConnectionGene`), if possible.
    ConnectionAdd,
    /// Removes an existing [ConnectionGene](`crate::connection_gene::ConnectionGene`), if
    /// possible.
    ConnectionRemove,
    /// Flips the value of the `enabled` flag of a [ConnectionGene](`crate::connection_gene::ConnectionGene`).
    ConnectionEnableDisable,
    /// Shifts the `weight` of a [ConnectionGene](`crate::connection_gene::ConnectionGene`) between
    /// the values of
    /// -[limit](`crate::connection_gene::CONNECTION_DEFAULT_WEIGHT_SHIFT_LIMITS`)..[limit](`crate::connection_gene::CONNECTION_DEFAULT_WEIGHT_SHIFT_LIMITS`).
    ConnectionShiftWeight,
    /// Randomizes the `weight` value of a [ConnectionGene](`crate::connection_gene::ConnectionGene`).
    ConnectionRandomizeWeight,
    /// Flits the sifn of the `weight` value of a [ConnectionGene](`crate::connection_gene::ConnectionGene`).
    ConnectionFlipWeightSign,
    /// Reconnects the [ConnectionNode](`crate::connection_gene::ConnectionGene`) from one  
    /// [NodeGene](`crate::node_gene::NodeGene`) to another, if possible.
    ConnectionReconnectToAnotherNode,
    /// Shifts the `weight` value of all [ConnectionGenes](`crate::connection_gene::ConnectionGene`).
    ConnectionsShiftAllWeights,
    /// Randomizes the `weight` value of all [ConnectionGenes](`crate::connection_gene::ConnectionGene`).
    ConnectionsRandomizeAllWeights,
    /// Flips the sign of the `weight` value of all [ConnectionGenes](`crate::connection_gene::ConnectionGene`).
    ConnectionsFlipAllWeightSigns,
}
impl fmt::Display for Mutation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{self:?}")
    }
}

impl Distribution<Mutation> for Standard {
    fn sample<R: Rng + ?Sized>(&self, randy: &mut R) -> Mutation {
        // FIX: Hardcoded size of `Mutation`'s size needs to be avoided.
        match randy.gen_range(0..=21) {
            0 => Mutation::NodeAdd,
            1 => Mutation::NodeInsertIntoConnection,
            2 => Mutation::NodeRemove,
            3 => Mutation::NodeShiftBias,
            4 => Mutation::NodeRandomizeBias,
            5 => Mutation::NodeFlipBiasSign,
            6 => Mutation::NodeChangeActivationFunction,
            7 => Mutation::NodesShiftAllBiases,
            8 => Mutation::NodesRandomizeAllBiases,
            9 => Mutation::NodesFlipAllBiasSigns,
            10 => Mutation::NodesChangeAllActivationFunctions,
            11 => Mutation::ConnectionAdd,
            12 => Mutation::ConnectionRemove,
            13 => Mutation::ConnectionEnableDisable,
            14 => Mutation::ConnectionShiftWeight,
            15 => Mutation::ConnectionRandomizeWeight,
            16 => Mutation::ConnectionFlipWeightSign,
            17 => Mutation::ConnectionReconnectToAnotherNode,
            18 => Mutation::ConnectionsShiftAllWeights,
            19 => Mutation::ConnectionsRandomizeAllWeights,
            _ => Mutation::ConnectionsFlipAllWeightSigns,
        }
    }
}
