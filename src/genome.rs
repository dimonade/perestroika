//! Aggregation of functionality of the [`Genome`].
//!
//! A [`Genome`] is an aggregation of [`NodeGene`]s and (optionally) [`ConnectionGene`]s.
//! Information from an input is propagated through the input [`Layer`] of the [`Genome`] to the
//! deeper [`Layer`]s, and retuned back to the user as a [`Vec`] of a size of the output [`Layer`].
//! [`ConnectionGene`]s are the way that the information is passing through the [`Genome`].
//!
//! A [`Genome`] is able to mutate.
//! Mutations are the engine of evolution, and randomness is what makes every [`Genome`] unique.
//!
//! A [`Genome`] may bear an "energetic cost", which is a sum of all of its parts, i.e.,
//! [`NodeGene`]s and [`ConnectionGene`]s.

use rand::seq::{IteratorRandom, SliceRandom};
use rand::{Rng, RngCore, SeedableRng};

use crate::connection_gene::{
    ConnectionGene, CONNECTION_DEFAULT_ENERGY_COST, CONNECTION_DEFAULT_WEIGHT_SHIFT_LIMITS,
};
use crate::connection_gene_builder::ConnectionGeneBuilder;
use crate::errors::PerestroikaError;
use crate::layer::Layer;
use crate::mutator::Mutation;
use crate::node_gene::{
    DepthType, NodeGene, NODE_DEFAULT_BIAS, NODE_DEFAULT_BIAS_SHIFT_LIMITS, NODE_DEFAULT_MASS,
};
use crate::node_gene_builder::NodeGeneBuilder;

use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt;
use std::rc::Rc;

/// A [`Genome`] contains a collection of [`NodeGene`]s and [`ConnectionGene`]s and controls their
/// interactions.
// TODO: Should Genomes have an `id` and `parent_id` to recreate their genealogical tree?
#[derive(Debug, Clone)]
pub struct Genome<T>
where
    T: SeedableRng,
{
    /// A [`Vec`] containing the [`Layer`]s which contain references to the [`NodeGene`]s.
    pub layers: Vec<Layer>,
    /// A running monotonically increasing counter that gives [`NodeGene`]s unique UUIDs.
    pub node_uuid: usize,
    /// A number of output [`NodeGene`]s in the [`Genome`] with the [`DepthType::Input`] tag.
    pub n_inputs: usize,
    /// A number of output [`NodeGene`]s in the [`Genome`] with the [`DepthType::Output`] tag.
    pub n_outputs: usize,
    /// A [`HashMap`] of [`ConnectionGene`]s with keys being (`source uuid`, `target uuid`).
    pub connections: HashMap<(usize, usize), ConnectionGene>,
    /// A random number generator for random mutation processes.
    pub rng: T,
    /// The current energetic cost of the genome.
    pub energy: Option<usize>,
}

impl<T> fmt::Display for Genome<T>
where
    T: SeedableRng,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Genome:\nLayers:\n{:?}", &self.layers)
    }
}

impl<T> Genome<T>
where
    T: SeedableRng + RngCore,
{
    /// Finds a [`NodeGene`] based on the UUID.
    ///
    /// # Arguments
    ///
    /// * `needle` - The UUID of the [`NodeGene`].
    pub fn find_node(&self, needle: usize) -> Option<Rc<RefCell<NodeGene>>> {
        for layer in &self.layers {
            for node in &layer.nodes {
                if node.borrow().index == needle {
                    return Some(Rc::clone(node));
                }
            }
        }
        None
    }

    /// Creates a new [`ConnectionGene`], if possible.
    ///
    /// # Arguments
    ///
    /// * `source_index` - The UUID of the source [`NodeGene`] .
    /// * `target_index` - The UUID of the target [`NodeGene`].
    /// * `weight` - The `weight` value of the [`ConnectionGene`].
    /// * `enabled` - The `enabled` status of the [`ConnectionGene`].
    /// * `energy_cost` - The energy cost of the [`ConnectionGene`].
    ///
    /// # Errors
    ///
    /// Creating a new [`ConnectionGene`]  will panic under the following scenarios:
    /// 1. The `source` UUID is missing.
    /// 2. The `target` UUID is missing.
    /// 3. The [`ConnectionGene`] already exists in the [`Genome`].
    /// 4. The `source` and `target` are on the same [`Layer`] (the depth is equal).
    /// 5. The `source` depth is deeper than the `target` depth ([`ConnectionGene`]s can only go
    ///    forward).
    pub fn create_connection(
        &mut self,
        source_index: usize,
        target_index: usize,
        weight: f64,
        enabled: bool,
        energy_cost: usize,
    ) -> Result<(), PerestroikaError> {
        let Some(source) = self.find_node(source_index) else {
            return Err(PerestroikaError::ConnectionGeneMissingSource);
        };
        let Some(target) = self.find_node(target_index) else {
            return Err(PerestroikaError::ConnectionGeneMissingTarget);
        };
        if self.connections.contains_key(&(source_index, target_index)) {
            return Err(PerestroikaError::ConnectionGeneAlreadyExists);
        }
        if source.borrow().depth == target.borrow().depth {
            return Err(PerestroikaError::ConnectionGeneSourceAndTargetSameDepth {
                depth: source.borrow().depth,
            });
        }
        if source.borrow().depth > target.borrow().depth {
            return Err(PerestroikaError::ConnectionGeneSourceDeeperThanTarget {
                source: source.borrow().depth,
                target: target.borrow().depth,
            });
        }

        target
            .borrow_mut()
            .connections_from
            .push(Rc::downgrade(&source));

        self.connections.insert(
            (source.borrow().index, target.borrow().index),
            ConnectionGeneBuilder::new()
                .with_source(source.borrow().index)
                .with_target(target.borrow().index)
                .with_weight(weight)
                .with_enabled_status(enabled)
                .with_energy_cost(energy_cost)
                .build()?,
        );

        Ok(())
    }

    /// Propagates the inputs through the [`Genome`] to return an output action.
    ///
    /// # Arguments
    ///
    /// * `inputs` - A reference to a slice containing the input values.
    ///
    /// # Errors
    ///
    /// If the input slice and the input layer have mismatched length, an error is returned.
    ///
    /// # Panics
    ///
    /// Should not panic.
    /// Panics if a [`ConnectionGene`] is not present in the [`Genome`]'s [`HashMap`].
    pub fn propagate(&mut self, inputs: &[f64]) -> Result<Vec<f64>, PerestroikaError> {
        if self.n_inputs != inputs.len() {
            return Err(PerestroikaError::LayerInputLengthMismatch);
        }
        // Layers are sorted by Input -> Hidden(n) -> Output.
        for layer in &self.layers {
            match layer.depth {
                // Input is different because it needs to get data from an external source.
                DepthType::Input => {
                    for (node, input) in layer.nodes.iter().zip(inputs) {
                        node.borrow_mut().mass = *input;
                        node.borrow_mut().activate_node();
                    }
                }
                // Hidden and Output are the same in the sense of pulling the masses.
                DepthType::Hidden(_) | DepthType::Output => {
                    for node in &layer.nodes {
                        let mut current_node = node.borrow_mut();
                        // Initialize new mass before starting to pull new masses.
                        let mut new_mass = 0.0;

                        for source in &current_node.connections_from {
                            let connection = self
                                .connections
                                .get(&(
                                    source.upgrade().expect("Source must exist.").borrow().index,
                                    current_node.index,
                                ))
                                .expect("Should be there.");
                            let source_node_mass =
                                source.upgrade().expect("Source must exist.").borrow().mass;

                            new_mass += source_node_mass * connection.weight;
                        }

                        current_node.mass = new_mass;
                        current_node.activate_node();
                    }
                }
            }
        }
        Ok(self
            .layers
            .last()
            .expect("Output layer must exist.")
            .nodes
            .iter()
            .map(|node| node.borrow().mass)
            .collect::<Vec<f64>>())
    }

    /// Mutates the [`Genome`].
    ///
    /// The mutation is chosen randomly.
    ///
    /// Mutating the [`Genome`] is what makes every entity unique and different.
    /// Different [`Genome`]s react in various ways to the same inputs.
    ///
    /// Mutations can act on a singe [`NodeGene`] or a [`ConnectionGene`], as well as on a variety
    /// or a collection of them at the same time.
    /// Currently the chances of all the mutations are the same, which is not optimal, since the
    /// mutations that alter *all* types are much more powerful.
    ///
    /// # Errors
    /// If a chosen mutation cannot be created, a relevantt error is propagated.
    /// For more details about the errors, check each mutation's documentation.
    pub fn mutate_randomly(&mut self) -> Result<(), PerestroikaError> {
        // TODO: Currently all mutations have the same chance, which is not ideal:
        // some mutations are much more powerful than others - `*_all_*` mutations.
        //
        // TODO: Perhaps it will make sense to take out this functionality into the Mutator, i.e.,
        // let m: Mutatior = Mutator::new();
        // m.node_add(genome);
        let new_mutation: Mutation = rand::random();
        println!("`mutate`: selected: {new_mutation}.");
        match new_mutation {
            Mutation::NodeAdd => self.mutate_node_add()?,
            // TODO: This mutation is not implemented and might be too strong.
            // Mutation::NodeInsertIntoConnection => self.mutate_node_insert_into_connection()?,
            Mutation::NodeRemove => self.mutate_node_remove()?,
            Mutation::NodeShiftBias => self.mutate_node_shift_bias()?,
            Mutation::NodeRandomizeBias => self.mutate_node_randomize_bias()?,
            Mutation::NodeFlipBiasSign => self.mutate_node_flip_bias_sign()?,
            Mutation::NodeChangeActivationFunction => {
                self.mutate_node_change_activation_function()?;
            }
            Mutation::NodesShiftAllBiases => self.mutate_nodes_shift_all_biases()?,
            Mutation::NodesRandomizeAllBiases => self.mutate_nodes_randomize_all_biases()?,
            Mutation::NodesFlipAllBiasSigns => self.mutate_nodes_flip_all_bias_signs()?,
            Mutation::NodesChangeAllActivationFunctions => {
                self.mutate_nodes_change_all_activation_functions()?;
            }
            Mutation::ConnectionAdd => self.mutate_connection_add()?,
            Mutation::ConnectionRemove => self.mutate_connection_remove()?,
            Mutation::ConnectionEnableDisable => self.mutate_conneciton_enable_disable()?,
            Mutation::ConnectionShiftWeight => self.mutate_conneciton_shift_weight()?,
            Mutation::ConnectionRandomizeWeight => {
                self.mutate_connection_randomize_weight()?;
            }
            Mutation::ConnectionFlipWeightSign => self.mutate_connection_flip_weight_sign()?,
            // TODO: This mutation is not implemented, and might be too strong.
            // Mutation::ConnectionReconnectToAnotherNode => {
            //     self.mutate_connection_reconnect_to_another_node()?,
            // }
            Mutation::ConnectionsShiftAllWeights => {
                self.mutate_connections_shift_all_weights()?;
            }
            Mutation::ConnectionsRandomizeAllWeights => {
                self.mutate_connections_randomize_all_weights()?;
            }
            Mutation::ConnectionsFlipAllWeightSigns => {
                self.mutate_connections_flip_all_weight_signs()?;
            }
            _ => {}
        };
        self.calculate_energy_usage();
        Ok(())
    }

    /// Checks whether hidden layers are present in the [`Genome`].
    ///
    /// The check is simply if there are more than 2 [`Layer`]s.
    fn are_hidden_layers_present(&self) -> bool {
        self.layers.len() > 2
    }

    /// Chooses a random hidden [`Layer`].
    ///
    /// # Errors
    ///
    /// If there are hidden [`Layer`]s in the [`Genome`] (at least one), an error is returned.
    fn choose_random_hidden_layer_mut(&mut self) -> Result<&mut Layer, PerestroikaError> {
        if !self.are_hidden_layers_present() {
            return Err(PerestroikaError::GenomeIsTooShallow);
        }
        if self.layers.len() - 2 - 1 == 0 {
            // `gen_range` cannot generate a value if there is no valid range.
            // -2 for Input and Output; -1 is for the only layer, which negates the range.
            Ok(&mut self.layers[1])
        } else {
            let random_layer_index = self.rng.gen_range(1..self.layers.len() - 2);
            Ok(&mut self.layers[random_layer_index + 1])
        }
    }

    /// Chooses a random [`NodeGene`] .
    fn choose_random_node(&mut self) -> &Rc<RefCell<NodeGene>> {
        self.layers
            .choose(&mut self.rng)
            .expect("Genome must have at least two Layers.")
            .nodes
            .choose(&mut self.rng)
            .expect("Layer must have at least one NodeGene.")
    }

    /// Adds a [`NodeGene`], if possible.
    ///
    /// The [`NodeGene`] comes with no [`ConnectionGene`]s, and can only be created on a [`Layer`]
    /// which is [`DepthType::Hidden`].
    /// When a new [`NodeGene`] is added the `node_uuid` running index is bumped.
    ///
    /// # Errors
    ///
    /// If the [`Genome`] has no hidden layer, an error is returned.
    pub fn mutate_node_add(&mut self) -> Result<(), PerestroikaError> {
        let new_node_uuid = self.node_uuid;
        let random_layer = self.choose_random_hidden_layer_mut()?;

        random_layer.nodes.push(Rc::new(RefCell::new(
            NodeGeneBuilder::new()
                .with_bias(NODE_DEFAULT_BIAS)
                .with_mass(NODE_DEFAULT_MASS)
                .with_node_type(random_layer.depth)
                .with_index(new_node_uuid)
                .build()?,
        )));
        self.node_uuid += 1;
        Ok(())
    }

    /// Removes a [`NodeGene`] from the [`Genome`], if possible.
    ///
    /// Removing a [`NodeGene`] also cleans up the [`ConnectionGene`]s that may have been connected
    /// to the chosen [`NodeGene`].
    ///
    /// # Errors
    ///
    /// Returns an error when the chosen hidden [`Layer`] has only a single [`NodeGene`]
    /// (empty [`Layer`]s are not supported at the moment).
    ///
    /// # Panics
    ///
    /// Should not panic as the random hidden layer should have 2 or more [`NodeGene`]s.
    pub fn mutate_node_remove(&mut self) -> Result<(), PerestroikaError> {
        // Only hidden layer node genes can be removed and only if the layer will not be empty.
        // To give the mutation more chance, it runs over layers with more than 1 node gene.
        let Some(random_hidden_layer) = self.layers[1..self.layers.len()]
            .iter()
            .filter(|x| x.nodes.len() > 1)
            .choose(&mut self.rng)
        else {
            return Err(PerestroikaError::LayerMustHaveMoreThanOneNodeGene);
        };

        // Choose a random NodeGene from the selected hidden layer.
        let node_gene_uuid = random_hidden_layer
            .nodes
            .choose(&mut self.rng)
            .expect("Must have more than one NodeGene.")
            .borrow()
            .index;

        // Remove the selected NodeGene from any `connections_from` vector.
        for layer in &self.layers {
            for node in &layer.nodes {
                node.borrow_mut().connections_from.retain(|x| {
                    x.upgrade().expect("NodeGene must exist.").borrow().index != node_gene_uuid
                });
            }
        }
        // Remove all the ConnectionGenes that originate from the chosen NodeGene;
        // Remove all the ConnectionGenes that point to the chosen NodeGene.
        self.connections.retain(|(source_uuid, target_uuid), _| {
            !((*source_uuid != node_gene_uuid) ^ (*target_uuid != node_gene_uuid))
        });

        // Drop the selected NodeGene.
        for layer in &mut self.layers {
            layer
                .nodes
                .retain(|node| node.borrow().index != node_gene_uuid);
        }
        Ok(())
    }

    /// Shifts the bias value of a chosen [`NodeGene`].
    ///
    /// # Errors
    ///
    /// Should not error, as there must be at least two [`NodeGene`]s.
    pub fn mutate_node_shift_bias(&mut self) -> Result<(), PerestroikaError> {
        let bias_shift = self
            .rng
            .gen_range(-NODE_DEFAULT_BIAS_SHIFT_LIMITS..NODE_DEFAULT_BIAS_SHIFT_LIMITS);
        let mut node = self.choose_random_node().borrow_mut();
        node.bias += bias_shift;
        node.bias = node.bias.clamp(-1.0, 1.0);

        Ok(())
    }

    /// Randomizes the bias value of a chosen [`NodeGene`].
    ///
    /// # Errors
    ///
    /// Should not error, as there must be at least two [`NodeGene`]s.
    pub fn mutate_node_randomize_bias(&mut self) -> Result<(), PerestroikaError> {
        self.choose_random_node().borrow_mut().bias = self.rng.gen_range(-1.0..1.0);

        Ok(())
    }

    /// Flips the bias value of a chosen [`NodeGene`].
    ///
    /// # Errors
    ///
    /// Should not error, as there must be at least two [`NodeGene`]s.
    pub fn mutate_node_flip_bias_sign(&mut self) -> Result<(), PerestroikaError> {
        self.choose_random_node().borrow_mut().bias *= -1.0;

        Ok(())
    }

    /// Chooses a different [actiavtion function](crate::node_gene::ActivationFunction)
    /// for the [`NodeGene`].
    ///
    /// # Errors
    ///
    /// Should not error, as there must be at least two [`NodeGene`]s.
    pub fn mutate_node_change_activation_function(&mut self) -> Result<(), PerestroikaError> {
        self.choose_random_node().borrow_mut().activation_function = rand::random();

        Ok(())
    }

    //  Shifts all biases of [`NodeGene`]s in the [`Genome`].
    ///
    /// # Errors
    ///
    /// Should not panic as there must be at least 2 [`NodeGene`]s.
    pub fn mutate_nodes_shift_all_biases(&mut self) -> Result<(), PerestroikaError> {
        for layer in &self.layers {
            for node in &layer.nodes {
                node.borrow_mut().bias += self
                    .rng
                    .gen_range(-NODE_DEFAULT_BIAS_SHIFT_LIMITS..NODE_DEFAULT_BIAS_SHIFT_LIMITS);
                node.borrow_mut().bias = node.borrow_mut().bias.clamp(-1.0, 1.0);
            }
        }
        Ok(())
    }

    /// Randomizes all biases of [`NodeGene`]s in the [`Genome`].
    ///
    /// # Errors
    ///
    /// Should not panic as there must be at least 2 [`NodeGene`]s.
    pub fn mutate_nodes_randomize_all_biases(&mut self) -> Result<(), PerestroikaError> {
        for layer in &self.layers {
            for node in &layer.nodes {
                node.borrow_mut().bias = self.rng.gen_range(-1.0..1.0);
            }
        }
        Ok(())
    }

    /// Flips all biases of [`NodeGene`]s in the [`Genome`].
    ///
    /// # Errors
    ///
    /// Should not panic as there must be at least 2 [`NodeGene`]s.
    pub fn mutate_nodes_flip_all_bias_signs(&mut self) -> Result<(), PerestroikaError> {
        for layer in &self.layers {
            for node in &layer.nodes {
                node.borrow_mut().bias *= -1.0;
            }
        }
        Ok(())
    }

    /// Changes all [activation functions](crate::node_gene::ActivationFunction)s in the [`Genome`].
    ///
    /// # Errors
    ///
    /// Cannot raise an error as there must be at least two [`NodeGene`]s in a [`Genome`]:
    /// 1. One input [`NodeGene`],
    /// 2. One output [`NodeGene`].
    ///
    pub fn mutate_nodes_change_all_activation_functions(&mut self) -> Result<(), PerestroikaError> {
        for layer in &self.layers {
            for node in &layer.nodes {
                node.borrow_mut().activation_function = rand::random();
            }
        }
        Ok(())
    }

    /// Choose a [`ConnectionGene`] mutably.
    ///
    /// # Errors
    ///
    /// If there are no [`ConnectionGene`]s in the [`Genome`], an error will be raised.
    fn choose_random_connection_mut(&mut self) -> Result<&mut ConnectionGene, PerestroikaError> {
        if self.connections.is_empty() {
            return Err(PerestroikaError::GenomeMissingConnectionGenes);
        }

        Ok(self
            .connections
            .values_mut()
            .choose(&mut self.rng)
            .expect("Genome must have a ConnectionGene at this point."))
    }

    /// Adds a new [`ConnectionGene`] to the [`Genome`].
    ///
    /// This function randomly chooses two[`Layer`]s and within these, two [`NodeGene`]s
    /// and if they do not share a [`ConnectionGene`], it creates one if possible.
    /// Therefore is not guaranteed that a new [`ConnectionGene`] will be created.
    ///
    /// See the [`create_connection`](`Genome::create_connection`) function for additional constraints.
    ///
    /// # Errors
    ///
    /// Returns an error if the proposed [`ConnectionGene`]  is not valid.
    ///
    /// # Examples
    ///
    /// ```
    /// use rand::{Rng, SeedableRng};
    /// use rand_chacha::ChaCha8Rng;
    ///
    /// use perestroika::{errors::PerestroikaError, genome::Genome, genome_builder::GenomeBuilder};
    ///
    /// let rng = rand_chacha::ChaCha8Rng::seed_from_u64(42);
    ///
    /// let mut genome: Genome<ChaCha8Rng> = GenomeBuilder::new()
    ///     .with_random_number_generator(rng)
    ///     .with_shape(&vec![1, 1])?
    ///     .build()?;
    /// // Force the mutation.
    /// genome.mutate_connection_add()?;
    ///
    /// # Ok::<(), PerestroikaError>(())
    ///
    /// ```
    pub fn mutate_connection_add(&mut self) -> Result<(), PerestroikaError> {
        let first_node_index = self.choose_random_node().borrow().index;
        let second_node_index = self.choose_random_node().borrow().index;

        let weight = self.rng.gen_range(-1.0..1.0);

        match self.create_connection(
            first_node_index,
            second_node_index,
            weight,
            true,
            CONNECTION_DEFAULT_ENERGY_COST,
        ) {
            Ok(()) => Ok(()),
            Err(e) => Err(e),
        }
    }

    /// Removes a random [`ConnectionGene`] from the [`Genome`] if any exist.
    ///
    /// # Errors
    ///
    /// Returns an error if there are no [`ConnectionGene`]s in the [`Genome`].
    ///
    /// # Panics
    ///
    /// Should not panic, as the [`ConnectionGene`] must exist, as well as the [`NodeGene`]s it
    /// connects.
    /// If does not exist, a custom error message is returned.
    ///
    /// # Examples
    ///
    /// ```
    /// use rand_chacha::ChaCha8Rng;
    ///
    /// use perestroika::{errors::PerestroikaError, genome::Genome, genome_builder::GenomeBuilder};
    ///
    /// let mut genome: Genome<ChaCha8Rng> = GenomeBuilder::new().with_shape(&vec![2, 2])?.build()?;
    /// // Create and remove ConnectionGenes.
    /// genome.create_connection(0, 2, 0.5, true, 0);
    /// assert_eq!(genome.connections.len(), 1);
    /// genome.mutate_connection_remove()?;
    /// assert_eq!(genome.connections.len(), 0);
    ///
    /// genome.create_connection(0, 2, 0.5, true, 0);
    /// assert_eq!(genome.connections.len(), 1);
    /// genome.create_connection(0, 3, 0.5, true, 0);
    /// assert_eq!(genome.connections.len(), 2);
    /// genome.mutate_connection_remove()?;
    /// assert_eq!(genome.connections.len(), 1);
    /// genome.mutate_connection_remove()?;
    /// assert_eq!(genome.connections.len(), 0);
    ///
    /// # Ok::<(), PerestroikaError>(())
    ///
    /// ```
    pub fn mutate_connection_remove(&mut self) -> Result<(), PerestroikaError> {
        let connection = self.choose_random_connection_mut()?;
        let source_uuid = connection.source;
        let target_uuid = connection.target;

        // The information about a connection must be removed from the target node as it will pull
        // the mass and weight using the source node.
        let Some(target_node) = self.find_node(target_uuid) else {
            return Err(PerestroikaError::UnhandledError {
                text: String::from("The target Node must exist."),
            });
        };
        // Remove the source node UUID from the target's connections_from vector.
        target_node
            .borrow_mut()
            .connections_from
            .retain(|source_node| {
                source_node
                    .upgrade()
                    .expect("NodeGene must exist.")
                    .borrow()
                    .index
                    != source_uuid
            });
        self.connections.remove(&(source_uuid, target_uuid));

        Ok(())
    }

    /// Flips the value of the `enabled` flag of a [`ConnectionGene`].
    ///
    /// # Errors
    ///
    /// Returns an error if the [`Genome`] does not contain [`ConnectionGene`]s.
    pub fn mutate_conneciton_enable_disable(&mut self) -> Result<(), PerestroikaError> {
        self.choose_random_connection_mut()?.enabled ^= true;
        Ok(())
    }

    /// Shifts the weight of a [`ConnectionGene`].
    ///
    /// # Errors
    ///
    /// Returns an error if the [`Genome`] does not contain [`ConnectionGene`]s.
    pub fn mutate_conneciton_shift_weight(&mut self) -> Result<(), PerestroikaError> {
        let weight_shift = self.rng.gen_range(
            -CONNECTION_DEFAULT_WEIGHT_SHIFT_LIMITS..CONNECTION_DEFAULT_WEIGHT_SHIFT_LIMITS,
        );
        let connection = self.choose_random_connection_mut()?;

        connection.weight += weight_shift;
        connection.weight = connection.weight.clamp(-1.0, 1.0);
        Ok(())
    }

    /// Randomizes the `weight` of a [`ConnectionGene`].
    ///
    /// # Errors
    ///
    /// Returns an error if the [`Genome`] does not contain [`ConnectionGene`]s.
    pub fn mutate_connection_randomize_weight(&mut self) -> Result<(), PerestroikaError> {
        self.choose_random_connection_mut()?.weight = self.rng.gen_range(-1.0..1.0);
        Ok(())
    }

    /// Flips the sign of the `weight` of a random [`ConnectionGene`] .
    ///
    /// # Errors
    ///
    /// Returns an error if the [`Genome`] does not contain [`ConnectionGene`]s.
    ///
    /// # Examples
    ///
    /// ```
    /// use rand_chacha::ChaCha8Rng;
    /// use perestroika::{errors::PerestroikaError, genome::Genome, genome_builder::GenomeBuilder};
    ///
    /// let mut genome: Genome<ChaCha8Rng> = GenomeBuilder::new().with_shape(&vec![1, 1])?.build()?;
    /// genome.create_connection(0, 1, 0.5, true, 0);
    /// genome.mutate_connection_flip_weight_sign()?;
    ///
    /// assert_eq!(genome.connections.get(&(0, 1)).expect("Created above.").weight, -0.5);
    ///
    /// # Ok::<(), PerestroikaError>(())
    ///
    /// ```
    pub fn mutate_connection_flip_weight_sign(&mut self) -> Result<(), PerestroikaError> {
        self.choose_random_connection_mut()?.weight *= -1.0;
        Ok(())
    }

    /// Shifts the weight of all [`ConnectionGene`]s in the [`Genome`].
    ///
    /// # Errors
    ///
    /// Returns an error if the [`Genome`] does not contain [`ConnectionGene`]s.
    pub fn mutate_connections_shift_all_weights(&mut self) -> Result<(), PerestroikaError> {
        for connection in self.connections.values_mut() {
            connection.weight += self.rng.gen_range(
                -CONNECTION_DEFAULT_WEIGHT_SHIFT_LIMITS..CONNECTION_DEFAULT_WEIGHT_SHIFT_LIMITS,
            );
            // The shift can kick the weight outside of the [-1.0..1.0], clamping brings it back.
            connection.weight = connection.weight.clamp(-1.0, 1.0);
        }
        Ok(())
    }

    /// Randomizes the weight of all [`ConnectionGene`]s in the [`Genome`].
    ///
    /// # Errors
    ///
    /// Returns an error if the [`Genome`] does not contain [`ConnectionGene`]s.
    pub fn mutate_connections_randomize_all_weights(&mut self) -> Result<(), PerestroikaError> {
        for connection in self.connections.values_mut() {
            connection.weight = self.rng.gen_range(-1.0..1.0);
        }
        Ok(())
    }

    /// Flips the weight sign of all [`ConnectionGene`]s in the [`Genome`].
    ///
    /// # Errors
    ///
    /// Returns an error if the [`Genome`] does not contain [`ConnectionGene`]s.
    pub fn mutate_connections_flip_all_weight_signs(&mut self) -> Result<(), PerestroikaError> {
        for connection in self.connections.values_mut() {
            connection.weight *= -1.0;
        }
        Ok(())
    }

    /// Calculates the total "energetic cost" of the [`Genome`].
    ///
    /// Each [`NodeGene`] and [`ConnectionGene`] may have an energetic cost associated with them.
    /// These costs are summed up. If there is no cost specified, it is set to 0.
    fn calculate_energy_usage(&mut self) {
        let mut new_energy = 0;
        for layer in &self.layers {
            for node in &layer.nodes {
                new_energy += node.borrow().energy_cost.unwrap_or(0);
            }
        }
        for connection in self.connections.values() {
            new_energy += connection.energy_cost.unwrap_or(0);
        }
        self.energy = Some(new_energy);
    }
}
#[cfg(test)]
mod test {
    use rand::SeedableRng;
    use rand_chacha::ChaCha8Rng;

    use crate::{errors::PerestroikaError, genome_builder::GenomeBuilder};

    use super::*;

    #[test]
    fn test_connections_can_not_go_backwards() -> Result<(), PerestroikaError> {
        let mut genome: Genome<ChaCha8Rng> = GenomeBuilder::new().with_shape(&[1, 1])?.build()?;
        assert!(genome.create_connection(1, 0, 0.0, true, 0).is_err());
        Ok(())
    }

    #[test]
    fn test_can_not_have_duplicated_connection() -> Result<(), PerestroikaError> {
        let mut genome: Genome<ChaCha8Rng> = GenomeBuilder::new().with_shape(&[1, 1])?.build()?;
        genome.create_connection(0, 1, 0.0, true, 0)?;
        assert!(matches!(
            genome.create_connection(0, 1, 0.1, true, 5),
            Err(PerestroikaError::ConnectionGeneAlreadyExists)
        ));
        Ok(())
    }

    /// Tests that a somewhat complex [`Genome`] executes [`Genome::mutate_node_remove`] correctly.
    #[test]
    fn test_mutation_node_remove_removes() -> Result<(), PerestroikaError> {
        let rng = rand_chacha::ChaCha8Rng::seed_from_u64(42);

        let mut genome: Genome<ChaCha8Rng> = GenomeBuilder::new()
            .with_shape(&[3, 2, 4])?
            .with_random_number_generator(rng)
            .build()?;

        genome.create_connection(0, 3, 0.1, true, 0)?;
        genome.create_connection(1, 3, 0.2, true, 0)?;
        genome.create_connection(3, 5, 0.3, true, 0)?;
        genome.create_connection(3, 6, 0.4, true, 0)?;
        genome.create_connection(3, 7, 0.5, true, 0)?;
        genome.create_connection(3, 8, 0.6, true, 0)?;
        // Test that all ConnectionGenes are established.
        assert_eq!(genome.connections.len(), 6);

        genome.mutate_node_remove()?;
        // Only a single node must remain in the hidden layer.
        assert_eq!(genome.choose_random_hidden_layer_mut()?.nodes.len(), 1);
        // All connections must be cleaned up.
        assert_eq!(genome.connections.len(), 0);
        // All Rcs must be cleaned up.
        assert!(genome.find_node(3).is_none());

        Ok(())
    }

    #[test]
    fn test_mutation_node_remove_does_not_remove_last_node_from_hidden_layer(
    ) -> Result<(), PerestroikaError> {
        let rng = rand_chacha::ChaCha8Rng::seed_from_u64(17);

        let mut genome: Genome<ChaCha8Rng> = GenomeBuilder::new()
            .with_shape(&[1, 1, 1, 1, 1])?
            .with_random_number_generator(rng)
            .build()?;
        assert!(matches!(
            genome.mutate_node_remove(),
            Err(PerestroikaError::LayerMustHaveMoreThanOneNodeGene)
        ));
        Ok(())
    }

    #[test]
    fn test_mutation_connection_remove_removes() -> Result<(), PerestroikaError> {
        let mut genome: Genome<ChaCha8Rng> = GenomeBuilder::new().with_shape(&[1, 1])?.build()?;

        genome.create_connection(0, 1, 0.0, true, 0)?;
        assert_eq!(genome.connections.len(), 1);

        genome.mutate_connection_remove()?;
        assert_eq!(genome.connections.len(), 0);
        Ok(())
    }
}
