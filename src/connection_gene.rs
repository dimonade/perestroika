//! Connection gene functionality aggregator.

use std::fmt;

/// Default value for the energy cost of a [`ConnectionGene`].
pub const CONNECTION_DEFAULT_ENERGY_COST: usize = 1;
/// Default limits for the range that the weight may be shifted in due to a mutation.
pub const CONNECTION_DEFAULT_WEIGHT_SHIFT_LIMITS: f64 = 0.2;

/// A [`ConnectionGene`] is a sturct contaning the information of a connection between two nodes in
/// a [Genome](crate::genome::Genome).
#[derive(Clone, Debug)]
pub struct ConnectionGene {
    /// The [`ConnectionGene`]'s source [NodeGene](crate::node_gene::NodeGene)'s UUID.
    pub source: usize,
    /// The [`ConnectionGene`]'s target [NodeGene](crate::node_gene::NodeGene)'s UUID.
    pub target: usize,
    /// The [`ConnectionGene`]'s weight.
    pub weight: f64,
    /// Marks whether the [`ConnectionGene`]' is enabled or disabled.
    pub enabled: bool,
    /// The [`ConnectionGene`]'s energy cost.
    pub energy_cost: Option<usize>,
}

impl fmt::Display for ConnectionGene {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "ConnectionGene, {:?}->{:?}, weight {:?}, enabled: {:?}.",
            self.source, self.target, self.weight, self.enabled
        )
    }
}
