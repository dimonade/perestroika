//! Genome builder functionality.

use std::{cell::RefCell, collections::HashMap, rc::Rc};

use rand::SeedableRng;

use crate::{
    connection_gene::ConnectionGene,
    errors::PerestroikaError,
    genome::Genome,
    layer::{Layer, LayerBuilder},
    node_gene::DepthType,
    node_gene_builder::NodeGeneBuilder,
};

/// See the documentation for [`Genome`].
#[allow(clippy::missing_docs_in_private_items)]
#[derive(Clone, Debug)]
pub struct GenomeBuilder<T>
where
    T: SeedableRng,
{
    nodes_uuid: usize,
    n_inputs: Option<usize>,
    n_outputs: Option<usize>,
    connections: HashMap<(usize, usize), ConnectionGene>,
    rng: T,
    layers: Vec<Layer>,
    energy: Option<usize>,
}

impl<T> Default for GenomeBuilder<T>
where
    T: SeedableRng,
{
    fn default() -> Self {
        Self {
            nodes_uuid: 0,
            n_inputs: Some(0),
            n_outputs: Some(0),
            connections: HashMap::new(),
            rng: T::seed_from_u64(17),
            layers: Vec::new(),
            energy: None,
        }
    }
}
impl<T> GenomeBuilder<T>
where
    T: SeedableRng + Clone,
{
    /// Create a default implementation of a [`Genome`].
    pub fn new() -> Self {
        Self::default()
    }

    /// Create a [`Genome`] with a specific shape.
    ///
    /// # Arguments
    ///
    /// * `shape` - A reference to a slice which describes how many
    /// [`NodeGene`](crate::node_gene::NodeGene)s are per each [`Layer`].
    ///
    /// # Errors
    ///
    /// Returns an error in the following scenarios:
    /// 1. The shape is has less than two elements (corresponding to an Input and Output)
    ///    [`Layer`].
    /// 2. If the input or output [`Layer`] have 0 [`NodeGene`](crate::node_gene::NodeGene)s.
    pub fn with_shape(&mut self, shape: &[usize]) -> Result<&mut Self, PerestroikaError> {
        if shape.len() < 2 {
            return Err(PerestroikaError::GenomeIsTooShallow);
        }

        for (layer_index, nodes) in shape.iter().enumerate() {
            if layer_index == 0 {
                // Input layer cannot be empty.
                if *nodes == 0 {
                    return Err(PerestroikaError::GenomeWithoutInputNodeGenes);
                }
                let mut layer = LayerBuilder::new().with_depth(DepthType::Input).build()?;
                self.n_inputs = Some(*nodes);
                for _ in 0..*nodes {
                    layer.nodes.push(Rc::new(RefCell::new(
                        NodeGeneBuilder::new()
                            .with_index(self.nodes_uuid)
                            .with_node_type(DepthType::Input)
                            .build()?,
                    )));
                    self.nodes_uuid += 1;
                }
                self.layers.push(layer);
            } else if layer_index == shape.len() - 1 {
                // Output layer cannot be empty.
                if *nodes == 0 {
                    return Err(PerestroikaError::GenomeWithoutOutputNodeGenes);
                }
                let mut layer = LayerBuilder::new().with_depth(DepthType::Output).build()?;
                self.n_outputs = Some(*nodes);
                for _ in 0..*nodes {
                    layer.nodes.push(Rc::new(RefCell::new(
                        NodeGeneBuilder::new()
                            .with_index(self.nodes_uuid)
                            .with_node_type(DepthType::Output)
                            .build()?,
                    )));
                    self.nodes_uuid += 1;
                }
                self.layers.push(layer);
            } else {
                let mut layer = LayerBuilder::new()
                    .with_depth(DepthType::Hidden(layer_index))
                    .build()?;
                for _ in 0..*nodes {
                    layer.nodes.push(Rc::new(RefCell::new(
                        NodeGeneBuilder::new()
                            .with_index(self.nodes_uuid)
                            .with_node_type(DepthType::Hidden(layer_index))
                            .build()?,
                    )));
                    self.nodes_uuid += 1;
                }
                self.layers.push(layer);
            }
        }
        Ok(self)
    }

    /// Add a custom random number generator to the [`Genome`].
    ///
    /// The RNG can be initiated with a seed for reproducibility.
    ///
    /// # Arguments
    ///
    /// * `rng` - a random number generator.
    ///
    /// # Examples
    /// ```
    /// use rand::SeedableRng;
    /// use rand_chacha::ChaCha8Rng;
    /// use perestroika::{errors::PerestroikaError, genome_builder::GenomeBuilder};
    ///
    /// let rng = rand_chacha::ChaCha8Rng::seed_from_u64(42);
    /// let genome = GenomeBuilder::new().with_random_number_generator(rng).build()?;
    ///
    /// # Ok::<(), PerestroikaError>(())
    /// ```
    pub fn with_random_number_generator(&mut self, rng: T) -> &mut Self {
        self.rng = rng;
        self
    }

    /// Add [`ConnectionGene`]s to the [`Genome`].
    pub fn with_connectinos(
        &mut self,
        connections: HashMap<(usize, usize), ConnectionGene>,
    ) -> &mut Self {
        // TODO: Currently this does not check the validity of the ConnectionGenes.
        self.connections = connections;
        self
    }

    /// Builds the [`Genome`].
    pub fn build(&self) -> Result<Genome<T>, PerestroikaError> {
        let Some(n_inputs) = self.n_inputs else {
            return Err(PerestroikaError::UnhandledError {
                text: String::from("Inputs must be specified."),
            });
        };
        let Some(n_outputs) = self.n_outputs else {
            return Err(PerestroikaError::UnhandledError {
                text: String::from("Outputs must be specified."),
            });
        };

        Ok(Genome {
            node_uuid: self.nodes_uuid,
            n_inputs,
            n_outputs,
            // TODO: Are clones necessary?
            layers: self.layers.clone(),
            connections: self.connections.clone(),
            rng: self.rng.clone(),
            energy: self.energy,
        })
    }
}
