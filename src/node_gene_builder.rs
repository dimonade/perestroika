//! Node gene builder functionality.

use std::{cell::RefCell, rc::Weak};

use crate::{
    errors::PerestroikaError,
    node_gene::{ActivationFunction, DepthType, NodeGene},
};

/// See the documentation for [`NodeGene`].
#[allow(clippy::missing_docs_in_private_items)]
#[derive(Clone)]
pub struct NodeGeneBuilder {
    index: Option<usize>,
    node_type: Option<DepthType>,
    energy_cost: Option<usize>,
    bias: Option<f64>,
    mass: Option<f64>,
    activation_function: Option<ActivationFunction>,
    connections_from: Vec<Weak<RefCell<NodeGene>>>,
}

impl Default for NodeGeneBuilder {
    fn default() -> Self {
        Self {
            index: None,
            node_type: None,
            energy_cost: Some(0),
            bias: Some(0.0),
            mass: Some(0.0),
            activation_function: Some(ActivationFunction::Identity),
            connections_from: Vec::new(),
        }
    }
}

impl NodeGeneBuilder {
    /// Create a default implementation of a [`NodeGene`].
    pub fn new() -> Self {
        Self::default()
    }

    /// Add a custom index to the [`NodeGene`].
    pub fn with_index(&mut self, index: usize) -> &mut Self {
        self.index = Some(index);
        self
    }

    /// Add a custom [`ConnectionGene`](crate::connection_gene::ConnectionGene) list to the
    /// [`NodeGene`].
    pub fn with_connections(&mut self, connectios_from: &[Weak<RefCell<NodeGene>>]) -> &mut Self {
        self.connections_from = connectios_from.to_vec();
        self
    }

    /// Add a custom [`DepthType`] value to the [`NodeGene`].
    pub fn with_node_type(&mut self, node_type: DepthType) -> &mut Self {
        self.node_type = Some(node_type);
        self
    }

    /// Add a custom [`ActivationFunction`] to the [`NodeGene`].
    pub fn with_activation_function(
        &mut self,
        activation_function: ActivationFunction,
    ) -> &mut Self {
        self.activation_function = Some(activation_function);
        self
    }

    /// Add a custom bias to the [`NodeGene`].
    pub fn with_bias(&mut self, bias: f64) -> &mut Self {
        self.bias = Some(bias);
        self
    }

    /// Add a custom mass value to the [`NodeGene`].
    pub fn with_mass(&mut self, mass: f64) -> &mut Self {
        self.mass = Some(mass);
        self
    }

    /// Build the [`NodeGene`].
    ///
    /// # Errors
    /// Raises an error in the following scenarios:
    /// 1. If the [`NodeGene`] is missing an index.
    /// 2. If the [`NodeGene`] is missing a depth value.
    pub fn build(&self) -> Result<NodeGene, PerestroikaError> {
        let Some(index) = self.index else {
            return Err(PerestroikaError::NodeGeneMissingIndex);
        };
        let Some(depth) = self.node_type else {
            return Err(PerestroikaError::NodeGeneMissingDepth);
        };

        Ok(NodeGene {
            index,
            depth,
            energy_cost: self.energy_cost,
            bias: self.bias.unwrap_or_default(),
            mass: self.mass.unwrap_or_default(),
            activation_function: self.activation_function.clone().unwrap_or_default(),
            connections_from: self.connections_from.clone(),
        })
    }
}
