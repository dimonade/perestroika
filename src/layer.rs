//! Layer functionality aggregator.  

use std::{cell::RefCell, fmt, rc::Rc};

use crate::{
    errors::PerestroikaError,
    node_gene::{DepthType, NodeGene},
};

/// See the documentation for [`Layer`].
#[allow(clippy::missing_docs_in_private_items)]
#[derive(Clone, Default)]
pub struct LayerBuilder {
    nodes: Vec<Rc<RefCell<NodeGene>>>,
    depth: Option<DepthType>,
}

impl LayerBuilder {
    /// Create a default implementation of [`Layer`].
    pub fn new() -> Self {
        Self::default()
    }

    /// Add [`NodeGene`]s to the [`Layer`].
    pub fn with_nodes(&mut self, nodes: &[Rc<RefCell<NodeGene>>]) -> &mut Self {
        self.nodes = nodes.to_vec();
        self
    }

    /// Specify the [`DepthType`] value of the [`Layer`].
    pub fn with_depth(&mut self, depth: DepthType) -> &mut Self {
        self.depth = Some(depth);
        self
    }

    /// Build the [`Layer`].
    ///
    /// # Errors
    ///
    /// If no [`DepthType`] is specified, an error is returned.
    pub fn build(&self) -> Result<Layer, PerestroikaError> {
        let Some(depth) = self.depth else {
            return Err(PerestroikaError::LayerMustHaveDepthType);
        };
        Ok(Layer {
            nodes: self.nodes.clone(),
            depth,
        })
    }
}

/// A struct containing the details about a [`Layer`].
#[derive(Clone, Debug)]
pub struct Layer {
    /// A collection of references to the [`NodeGene`]s that are present in the [`Layer`].
    pub nodes: Vec<Rc<RefCell<NodeGene>>>,
    /// The [`DepthType`] of the [`Layer`].
    pub depth: DepthType,
}

impl fmt::Display for Layer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO: No display yet.
        write!(f, "TODO: No display for Layer yet.")
    }
}

impl Layer {}
