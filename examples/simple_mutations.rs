use perestroika::{errors::PerestroikaError, genome::Genome, genome_builder::GenomeBuilder};
use rand_chacha::ChaCha8Rng;

fn main() -> Result<(), PerestroikaError> {
    // Let's construct a new Genome, which will undergo some of the available mutations.
    let mut genome: Genome<ChaCha8Rng> = GenomeBuilder::new()
        .with_shape(&vec![2, 3, 3, 2])?
        .build()?;
    // It is possible to force an explicit mutation on the `Genome`, unless it is impossible by
    // using a convenience function:
    // Add a connection.
    genome.create_connection(0, 2, 0.5, true, 1)?;
    println!("{genome:#?}");

    // Or by activating the mutation directly:
    genome.mutate_node_add()?;
    println!("{genome:#?}");

    // But the more natural way would be to call the `mutate_randomly` method of the `Genome`:
    genome.mutate_randomly()?;
    // This will choose a random mutation from a list of implemented mutations.

    Ok(())
}
