use perestroika::{
    connection_gene_builder::ConnectionGeneBuilder,
    errors::PerestroikaError,
    node_gene::{ActivationFunction, DepthType, NodeGene},
    node_gene_builder::NodeGeneBuilder,
};

fn main() -> Result<(), PerestroikaError> {
    // Prepare two [`NodeGene`]s to connect to:
    let node_one: NodeGene = NodeGeneBuilder::new()
        .with_node_type(DepthType::Input)
        .with_index(0)
        .with_activation_function(ActivationFunction::ReLU)
        .build()?;
    let node_two: NodeGene = NodeGeneBuilder::new()
        .with_node_type(DepthType::Output)
        .with_index(9)
        .with_activation_function(ActivationFunction::LeakyReLU)
        .build()?;

    // Construct a single [`ConnectionGene`] at a time:
    let first_connection_gene = ConnectionGeneBuilder::new()
        .with_source(node_one.index)
        .with_target(node_two.index)
        .with_enabled_status(true)
        .build()?;
    println!("{first_connection_gene:#?}");

    // Note that when a `ConnectionGene` is constructed with the `with_source` or `with_target`
    // functions, the `ConnectionGene` makes no attempt to check the if the index is correct.
    // This is a task that is either handled through the `Genome`, or the user's responsibility.
    let second_connection_gene = ConnectionGeneBuilder::new()
        .with_source(42)
        .with_target(666)
        .with_enabled_status(false)
        .build()?;
    println!("{second_connection_gene:#?}");

    Ok(())
}
