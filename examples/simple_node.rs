use perestroika::{
    errors::PerestroikaError,
    node_gene::{ActivationFunction, DepthType, NodeGene},
    node_gene_builder::NodeGeneBuilder,
};

fn main() -> Result<(), PerestroikaError> {
    // Construct a single [`NodeGene`] at a time:
    // One.
    let first_node_gene = NodeGeneBuilder::new()
        .with_index(0)
        .with_node_type(DepthType::Input)
        .with_activation_function(ActivationFunction::Sigmoid)
        .build()?;
    println!("{first_node_gene:#?}");

    // Two.
    let second_node_gene = NodeGeneBuilder::new()
        .with_index(1)
        .with_node_type(DepthType::Output)
        .with_bias(0.5)
        .with_activation_function(ActivationFunction::Tanh)
        .build()?;
    println!("{second_node_gene:#?}");

    // Or create them iteratively.
    let mut node_genes: Vec<NodeGene> = Vec::new();
    for (index, bias) in vec![0.0, 0.5, 1.0].iter().enumerate() {
        node_genes.push(
            NodeGeneBuilder::new()
                .with_index(index)
                .with_node_type(DepthType::Input)
                .with_bias(*bias)
                .build()?,
        );
    }
    println!("{node_genes:#?}");
    Ok(())
}
