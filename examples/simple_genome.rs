use perestroika::{errors::PerestroikaError, genome::Genome, genome_builder::GenomeBuilder};
use rand_chacha::ChaCha8Rng;

fn main() -> Result<(), PerestroikaError> {
    // Note that in all cases below, the `Genomes` are created without `ConnectionGene`s.
    let in_out_genome: Genome<ChaCha8Rng> =
        GenomeBuilder::new().with_shape(&vec![1, 1])?.build()?;
    println!("{in_out_genome}");

    let more_complex_genome: Genome<ChaCha8Rng> = GenomeBuilder::new()
        .with_shape(&vec![5, 4, 3, 4, 5])?
        .build()?;
    println!("{more_complex_genome:#?}");

    Ok(())
}
